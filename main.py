import asyncio
import os

from src.server import Server

if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    port = os.environ.get("MINES_PORT", 8080)
    game_server = Server(port, event_loop)
    game_server.start()
    event_loop.run_forever()
