FROM python:3.8

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY src ./src
COPY main.py ./
COPY run.sh ./
COPY index.html ./
COPY script.js ./
COPY assets ./assets

ENV TZ=Europe/Paris

CMD [ "./run.sh" ]
