import pytest

from src.grid import Grid


@pytest.fixture
def grid():
    return Grid(20, 10, 10)


def test_grid(grid):
    cell = grid.get_cell(10, 5)
    assert cell.x == 10
    assert cell.y == 5
    assert cell.coordinates == (10, 5)

    coordinates = [(9, 4), (10, 4), (11, 4), (9, 5), (11, 5), (9, 6), (10, 6), (11, 6)]
    neighbours = grid.neighbour_of(cell)
    assert len(neighbours) == 8
    for n in neighbours:
        assert n.coordinates in coordinates

    coordinates = [(0, 1), (1, 0), (1, 1)]
    neighbours = grid.neighbour_of(grid.get_cell(0, 0))
    assert len(neighbours) == 3
    for n in neighbours:
        assert n.coordinates in coordinates


@pytest.mark.parametrize("x, y, nb_mines, nb_mines_expected, first_cell_x, first_cell_y",
                         [(20, 10, 10, 10, 10, 5),
                          (10, 10, 10, 10, 5, 5),
                          (10, 10, 999, 91, 5, 5),
                          (10, 10, 999, 94, 0, 1),
                          (10, 10, 0, 0, 5, 5)])
def test_generation(x, y, nb_mines, nb_mines_expected, first_cell_x, first_cell_y):
    grid = Grid(x, y, nb_mines)
    assert len([c for c in grid.cells(mined=True)]) == 0
    grid.generate(grid.get_cell(first_cell_x, first_cell_y))
    assert len([c for c in grid.cells(mined=True)]) == nb_mines_expected
    assert grid.nb_mines == nb_mines_expected


def test_value():
    grid = Grid(20, 10, 999)
    corner = grid.get_cell(0, 0)
    grid.generate(corner)
    assert corner.value == 0
    assert grid.get_cell(5, 5).value == 8

    grid = Grid(10, 10, 0)
    grid.get_cell(0, 0).mined = True
    grid.get_cell(1, 0).mined = True
    assert grid.get_cell(1, 1).value == 2
