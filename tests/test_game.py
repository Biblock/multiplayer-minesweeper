from src.game import Game


def test_turns():
    game = Game()
    p1, p2 = game.player1, game.player2
    for i in range(10):
        assert game.current_player == p1
        game.next_player()
        assert game.current_player == p2
        game.next_player()


def test_game():
    game = Game()
    p1, p2 = game.player1, game.player2
    assert game.current_player == p1
    game.discover(8, 8)
    mined = game.grid.cells(mined=True)
    assert game.current_player == p2
    assert p1.score == p2.score == 0

    game.discover(*mined[0].coordinates)
    assert p2.score == 1
    assert game.current_player == p2
    game.discover(*mined[1].coordinates)
    assert p2.score == 2
    assert game.current_player == p2

    game.discover(*next(c.coordinates for c in game.grid.cells(mined=False, discovered=False)))
    assert p2.score == 2
    assert game.current_player == p1
