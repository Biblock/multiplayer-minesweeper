var app = new Vue({
  el: '#app',
  data: {
    socket: null,
    ready: false,
    game_create_response_id: null,
    game_id: null,
    player_id: null,
    game: null,
  },
  computed: {
    urls: function() {
      base = location.protocol + '//' + location.host + location.pathname
      return [
        base + "?game_id=" + this.game_id + "&player_id=player_1",
        base + "?game_id=" + this.game_id + "&player_id=player_2"
      ]
    },
    is_player_1: function() {
      return this.player_id == "player_1"
    }
  },
  methods: {
    async copy(url) {
      try {
        await navigator.clipboard.writeText(url)
      } catch($e) {
        alert('Cannot copy')
      }
    },
    get_img: function(cell) {
      img = "/mines/assets/32x32/"
      if (!cell.discovered) {
        img += "unknown_2"
      } else if (cell.mined) {
        img += "bomb_exploded"
      } else {
        img += cell.value
      }
      return img + ".png"
    },
    subscribe: function() {
      this.send({
      "action": "SUBSCRIBE",
      "game_id": this.game_id,
      "player_id": this.player_id
      })
    },
    socketOnCreated: function() {
      queryString = window.location.search
      urlParams = new URLSearchParams(queryString)
      this.game_id = urlParams.get("game_id")
      this.player_id = urlParams.get("player_id")
      if (this.game_id) {
        this.subscribe()
      }
      this.ready = true
    },
    create_game: function() {
      this.send({"action": "CREATE_GAME"})
    },
    click: function(cell) {
      this.send({"action": "DISCOVER", "game_id": this.game_id, "player_id": this.player_id, "coordinates": {"x": cell.x, "y": cell.y}})
    },
    send: function(data) {
      console.log(data)
      this.socket.send(JSON.stringify(data))
    },
    socketOnmessage: function(event) {
      event = JSON.parse(event.data).data
      console.log(event)
      if (event.event === "GAME_CREATED") {
        this.game_id = event.game_id
      }
      if (event.event === "SUBSCRIBED") {
        this.game = event.game
      }
      if (event.event === "DISCOVERED") {
        this.game = event.game
      }
    },
  },
  created: function() {
//    this.socket = new WebSocket("ws://localhost:8080")
    this.socket = new WebSocket("wss://biblock.fr/ws_mines")
    this.socket.onmessage = this.socketOnmessage
    this.socket.onopen = this.socketOnCreated
  }
})
