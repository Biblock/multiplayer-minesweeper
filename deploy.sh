PORT="${MINES_PORT:-80}"
TAG="${MINES_TAG:-latest}"
CONTAINER_PORT="${CONTAINER_PORT:-8890}"

echo "Build ? y/n"
read ans
[[ $ans == "y" ]] && ./build.sh

container_name=mines_"$TAG"
docker rm -f $container_name
docker run -d -e PYTHONUNBUFFERED=1 --name $container_name -p "$CONTAINER_PORT":"$PORT" -p 7071:5555 -e MINES_PORT="$PORT" mines:"$TAG"
