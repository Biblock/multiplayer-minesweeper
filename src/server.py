import asyncio
import json

import websockets

from src.commands import Commands
from src.exceptions import InvalidJSonError, MinesweeperError


class Server:
    def __init__(self, port, loop):
        self.port = port
        self.event_loop = loop
        self.games = []
        self.events = []

    def start(self):
        # noinspection PyTypeChecker
        start_server = websockets.serve(self.main_loop, "0.0.0.0", self.port)
        self.event_loop.run_until_complete(start_server)

    async def main_loop(self, websocket, _):
        lock = asyncio.Lock()
        try:
            async for message in websocket:
                async with lock:
                    self.process_message(websocket, message)
                    await self.send_events()
        finally:
            await self.send_events()

    def process_message(self, websocket, message):
        try:
            try:
                print(message)
                json_data = json.loads(message)
            except json.JSONDecodeError:
                raise InvalidJSonError
            command = Commands(json_data, self, websocket)
            command.execute()
        except MinesweeperError as e:
            print("Oops...", e.__class__.__name__)

    async def send_events(self):
        for event in self.events:
            await event.send()
        self.events = []
