from random import shuffle

from src.exceptions import CellOutOfGridError


class Cell:
    def __init__(self, grid, x, y):
        self.mined = False
        self.exploded = False
        self.discovered = False
        self.x = x
        self.y = y
        self.grid = grid

    def __str__(self) -> str:
        return f"({self.x}; {self.y})"

    @property
    def value(self):
        return len([cell for cell in self.grid.neighbour_of(self) if cell.mined])

    @property
    def coordinates(self):
        return self.x, self.y

    def discover(self):
        if not self.grid.generated:
            self.grid.generate(self)
        if self.discovered:
            return
        if self.mined:
            self.exploded = True
        self.discovered = True
        if not self.mined and self.value == 0:
            for cell in self.grid.neighbour_of(self):
                cell.discover()

    def to_json(self):
        obj = {
            "discovered": self.discovered,
            "x": self.x,
            "y": self.y
        }
        if self.discovered:
            obj["mined"] = self.mined
            obj["value"] = self.value

        return obj


class Grid:
    def __init__(self, width, height, nb_mines):
        self.nb_mines = nb_mines
        self.generated = False
        self.width = width
        self.height = height
        self.grid = [[Cell(self, i, j) for j in range(height)] for i in range(width)]

    def cells(self, mined=None, discovered=None):
        return [cell for col in self.grid for cell in col
                if mined is None or cell.mined == mined
                and discovered is None or cell.discovered == discovered]

    @property
    def undiscovered(self):
        return self.cells(discovered=False)

    @property
    def over(self):
        mined = self.cells(mined=True)
        return bool(mined and all(cell.discovered for cell in mined))

    def generate(self, first_cell):
        no_mines_cells_coordinates = [c.coordinates for c in [first_cell, *self.neighbour_of(first_cell)]]

        cells = [cell for cell in self.cells() if cell.coordinates not in no_mines_cells_coordinates]
        self.nb_mines = min(self.nb_mines, len(cells))

        shuffle(cells)
        for c in cells[:self.nb_mines]:
            c.mined = True
        self.generated = True

    def neighbour_of(self, cell):
        n_cells = []
        for i in range(cell.x - 1, cell.x + 2):
            for j in range(cell.y - 1, cell.y + 2):
                try:
                    n_cell = self.get_cell(i, j)
                except CellOutOfGridError:
                    pass
                else:
                    if n_cell != cell:
                        n_cells.append(n_cell)
        return n_cells

    def get_cell(self, x, y):
        if x < 0 or y < 0:
            raise CellOutOfGridError
        else:
            try:
                return self.grid[x][y]
            except IndexError:
                raise CellOutOfGridError

    def to_json(self):
        return [[self.grid[x][y].to_json() for x in range(self.width)] for y in range(self.height)]
