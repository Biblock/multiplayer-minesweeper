class MinesweeperError(Exception):
    pass


class CellOutOfGridError(MinesweeperError):
    pass


class AlreadyDiscoveredError(MinesweeperError):
    pass


# server
class InvalidJSonError(MinesweeperError):
    pass


class MissingArgumentError(MinesweeperError):
    pass


class NoSuchActionError(MinesweeperError):
    pass


class NoSuchGameError(MinesweeperError):
    pass


class InvalidPlayerError(MinesweeperError):
    pass


class WrongPlayerTurnError(MinesweeperError):
    pass
