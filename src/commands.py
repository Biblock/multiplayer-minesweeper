from src.event import Event
from src.exceptions import MissingArgumentError, NoSuchActionError, NoSuchGameError, InvalidPlayerError, \
    WrongPlayerTurnError
from src.server_game import ServerGame

COMMANDS = {}


class ArgumentParser:
    def __init__(self, data, server, websocket):
        self.data = data
        self.server = server
        self.websocket = websocket

    def get_arguments(self, *keys, nullable=None):
        args = []
        nullable = [] if nullable is None else nullable
        for key in keys:
            args.append(self.get_argument(key, key in nullable))
        return args

    def get_argument(self, key, nullable=False):
        try:
            return self.data[key]
        except KeyError:
            if nullable:
                return None
            else:
                raise MissingArgumentError

    def get_game(self):
        game_id = self.get_argument("game_id")
        try:
            game = next(game for game in self.server.games if game.id == game_id)
        except StopIteration:
            raise NoSuchGameError
        return game


def action(command_name, game_action=False, target=False):
    def deco(function):
        COMMANDS[command_name] = {
            "func": function,
            "game_action": game_action,
            "target": target
        }

        return function

    return deco


class Commands:
    def __init__(self, json_data, server, websocket):
        self.parser = ArgumentParser(json_data, server, websocket)
        self.server = server
        self.websocket = websocket

    def execute(self):
        act = self.parser.get_argument("action")
        args = [self]
        kwargs = {}
        try:
            command = COMMANDS[act]
        except KeyError:
            raise NoSuchActionError
        if command["game_action"]:
            kwargs["game"] = self.parser.get_game()
            kwargs["player_id"] = self.parser.get_argument("player_id")
        command["func"](*args, **kwargs)

    @action("CREATE_GAME")
    def create_game(self):
        new_game = ServerGame(self.server)
        self.server.events.append(Event([self.websocket], {
            "event": "GAME_CREATED",
            "game_id": new_game.id
        }))
        self.server.games.append(new_game)

    @action("SUBSCRIBE", game_action=True)
    def subscribe(self, game, player_id):
        if player_id == "player_1":
            game.player_1_ws = self.websocket
        elif player_id == "player_2":
            game.player_2_ws = self.websocket
        else:
            raise InvalidPlayerError
        self.server.events.append(Event([self.websocket], {
            "event": "SUBSCRIBED",
            "game": game.to_json()
        }))

    @action("DISCOVER", game_action=True)
    def discover(self, game, player_id):
        print(player_id)
        coordinates = self.parser.get_argument("coordinates")
        x, y = coordinates["x"], coordinates["y"]
        game.discover(x, y, player_id)
        self.server.events.append(Event(game.players_ws, {
            "event": "DISCOVERED",
            "game": game.to_json()
        }))
        if game.over:
            self.server.events.append(Event(game.players_ws, {
                "event": "GAME_OVER",
                "game": game.to_json()
            }))
            self.server.games.remove(game)
