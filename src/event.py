import json

import websockets


class Event:
    def __init__(self, recipients, payload):
        self.recipients = recipients
        self.payload = payload

    async def send(self):
        for recipient in self.recipients:
            try:
                await recipient.send(json.dumps({"data": self.payload}))
                print(self.payload)
            except websockets.ConnectionClosed:
                print(f"Connection closed {recipient.id}")
            except AttributeError as e:
                print(f"AttributeError {recipient.id}. This should never happen...")
                raise e

    def __repr__(self):
        return json.dumps(self.payload)
