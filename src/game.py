from itertools import cycle

from src.exceptions import AlreadyDiscoveredError
from src.grid import Grid


class Player:
    def __init__(self):
        self.score = 0


class Game:
    # def __init__(self, width=5, height=5, nb_mines=10):
    def __init__(self, width=32, height=16, nb_mines=160):
        self.grid = Grid(width, height, nb_mines)
        self.player1 = Player()
        self.player2 = Player()
        self.turns = cycle([self.player1, self.player2])
        self.current_player = next(self.turns)

    def next_player(self):
        self.current_player = next(self.turns)

    def discover(self, x, y):
        new_grid = not any(cell.discovered for cell in self.grid.cells())
        cell = self.grid.get_cell(x, y)
        if cell.discovered:
            raise AlreadyDiscoveredError
        cell.discover()
        if cell.mined:
            self.current_player.score += 1
        else:
            if not new_grid:
                self.next_player()

    def to_json(self):
        return {
            "score": {
                "player_1": self.player1.score,
                "player_2": self.player2.score
            },
            "turn_of": "player_1" if self.current_player == self.player1 else "player_2",
            "over": self.grid.over,
            "grid": self.grid.to_json(),
            "remaining_mines": len([cell for cell in self.grid.cells(mined=True) if not cell.discovered]),
            "total_mines": self.grid.nb_mines
        }
