from uuid import uuid4

from src.event import Event
from src.exceptions import WrongPlayerTurnError
from src.game import Game


class Identified:
    def __init__(self):
        self.id = str(uuid4())


class GameObserver:
    def add_event(self, payload):
        raise NotImplementedError


class ServerGame(Identified, GameObserver):
    def __init__(self, server):
        super().__init__()
        self.engine_game = Game()
        self.player_1_ws = None
        self.player_2_ws = None
        self.server = server

    @property
    def players_ws(self):
        return [p for p in (self.player_1_ws, self.player_2_ws) if p is not None]

    @property
    def over(self):
        return self.engine_game.grid.over

    def add_event(self, payload):
        self.server.events.append(Event(self.players_ws, payload))

    def to_json(self):
        return {
            "game_id": self.id,
            "game_status": self.engine_game.to_json()
        }

    def discover(self, x, y, player_id):
        if self.engine_game.to_json()["turn_of"] != player_id:
            raise WrongPlayerTurnError
        self.engine_game.discover(x, y)